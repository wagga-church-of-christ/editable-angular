import {
  Component, OnInit, AfterViewInit, ViewChild, ElementRef
} from '@angular/core';

import { PromiseDelegate } from '@phosphor/coreutils';

import * as  MarkdownIt from 'markdown-it';

@Component({
  selector: 'app-dynamic-container',
  template: `<div #container></div>`
})
export class DynamicContainerComponent implements OnInit, AfterViewInit {
  myMarkdownIt: MarkdownIt.MarkdownIt;
  url = 'https://gitlab.com/api/v4/projects/8327344/repository/files/pages%2Fblog%2Ftest.md/raw?ref=master';
  markdownLoaded = new PromiseDelegate<string>();

  @ViewChild('container', { read: ElementRef })
  container: ElementRef<HTMLDivElement>;

  ngOnInit() {
    this.getMarkdownFile()

    this.myMarkdownIt = new MarkdownIt({
      html: true,
      linkify: true,
      typographer: true,
    });
    this.myMarkdownIt.disable('code')
  }

  ngAfterViewInit() {
    this.markdownLoaded.promise.then(markdown => {
      const html = this.myMarkdownIt.render(markdown);
      this.container.nativeElement.innerHTML = html;
    })
  }

  private getMarkdownFile() {
    let c = new XMLHttpRequest()
    c.onload = () => {
      this.markdownLoaded.resolve(c.response)
    }
    c.open("GET", this.url);
    c.send();
  }
}
